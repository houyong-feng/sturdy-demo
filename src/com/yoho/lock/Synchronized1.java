package com.yoho.lock;


/**
 * 了解 Synchronized 原理
 */
public class Synchronized1 {


    /**
     * 查看字节代码
     * 命令： javap.exe -v com.yoho.lock.Synchronized1
     *
     * 参考资料：
     * https://docs.oracle.com/javase/specs/jvms/se11/html/jvms-3.html#jvms-3.14(官网字节码解释)
     * https://docs.oracle.com/javase/specs/jvms/se11/html/jvms-2.html#jvms-2.11.10（官网同步化解释 synchronized）
     * https://docs.oracle.com/javase/specs/jvms/se11/html/jvms-4.html#jvms-4.6（官网method_info内容 ACC_SYNCHRONIZED）
     *
     *
     * 对象锁与类锁中都有 monitorenter 与 monitorexit 两个
     *  监视之前
     *  0: aload_0
     *  1: dup
     *  2: astore_1
     *  3: monitorenter  // 获取监视器
     *  19: monitorexit  // 解除监视器
     *  指令执行到monitorenter
     *  开启0-2对象的监视器，并将ACC_SYNCHRONIZED 设置为1 ，
     *  指令执行monitorexit
     *  解除之前的对象监视器 计数器减一 ACC_SYNCHRONIZED  为0
     *  在ACC_SYNCHRONIZED 为 1 的时候新进来的线程校验失败，进入 BLOCKED 状态
     *
     * 方法锁中有  " flags: ACC_PUBLIC, ACC_SYNCHRONIZED "
     * 与上面相同ACC_SYNCHRONIZED来控制单线程执行
     */
    public static void main(String[] args) {
        new Object();
    }
    /**
     * 方法锁
     */
    public synchronized  void methodLock(){
        System.out.println("这是方法锁");
    }

    /**
     * 锁
     */
    public void classLock(){
        synchronized (this){
            System.out.println("这是类锁");
        }
    }

    /**
     * 对象锁
     */
    public void objectLock(){
        synchronized (new Object()){
            System.out.println("这是对象锁");
        }
    }



}

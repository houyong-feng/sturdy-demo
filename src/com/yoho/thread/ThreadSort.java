package com.yoho.thread;

/**
 * 线程顺序执行
 * 线程 t1,t2,t3 按顺序执行
 */
public class ThreadSort {
    /**
     * 理解Thread中的join()方法
     *
     * 当线程A正在执行的时候，任务中有B.join()出现，暂停当前线程返回B线程并执行B线程，B执行完成后再执行A剩余任务
     *
     * @param args
     */
    public static void main(String[] args) throws InterruptedException {

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("T2执行");
            }
        });
        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("T3执行");
            }
        });

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("T1执行:开始");
                try {
                    // 线程插队 先执行
                    t2.join();
                    t3.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("T1执行：结束");
            }
        });

        t1.start();
        t2.start();
        t3.start();
        t1.join();
        System.out.println("主线程执行完毕");

    }
}
